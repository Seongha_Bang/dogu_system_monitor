# Dogu System Monitor


컴퓨터의 각종 상태 정보들을 ROS 상에서 토픽으로 보내주는 노드


## Library packages installation


```
bash setup/install.sh
```


## 실행


start.launch 파일로 실행


- 파라미터
  - system_monitoring_frequency: 토픽 발행 간격 = Hz (변수 - frequency)

## 사용법


노드가 실행되면 자동으로 시스템 상태를 측정하기 시작한다.   
상태 정보는 크게 CPU, RAM, Disk, Network, Wifi, 시간 정보 등이 있다.


### 이름


 - 노드: /system_monitor
 - 토픽: /system_monitor


### Topic - /system_monitor


기본적으로는 1Hz로 발행되지만,   
start.launch 파일을 실행할 때 파라미터로 설정할 수 있지만,   
인터넷 정보는 고정적으로 1분에 한 번씩만 업데이트 된다.   
또한 실행 중 파라미터의 값이 변경되면 노드에도 반영된다.

 - cpu
   - percent: 전체 사용률 [%]
   - frequency: 주파수
     - current: 현재 [MHz]
     - max: 최대 [MHz]
     - min: 최소 [MHz]
   - core: 코어 각각의 정보 (배열)
     - percent: 사용률 [%]
     - frequency: 주파수
       - current: 현재 [MHz]
       - max: 최대 [MHz]
       - min: 최소 [MHz]
 - ram
   - active: 현재 또는 최근에 사용 된 크기
     - value: 값 [실수 (float)]
     - unit: 단위 [문자열]
     - raw: 가공되지 않은 정보 [Byte]
   - available: 즉시 사용 가능한 크기
     - value: 값 [실수 (float)]
     - unit: 단위 [문자열]
     - raw: 가공되지 않은 정보 [Byte]
   - buffers: 캐시 (파일 시스템 등)
     - value: 값 [실수 (float)]
     - unit: 단위 [문자열]
     - raw: 가공되지 않은 정보 [Byte]
   - cached: 캐시
     - value: 값 [실수 (float)]
     - unit: 단위 [문자열]
     - raw: 가공되지 않은 정보 [Byte]
   - free: 여유 공간
     - value: 값 [실수 (float)]
     - unit: 단위 [문자열]
     - raw: 가공되지 않은 정보 [Byte]
   - inactive: 사용되지 않은 메모리
     - value: 값 [실수 (float)]
     - unit: 단위 [문자열]
     - raw: 가공되지 않은 정보 [Byte]
   - percent: 사용량 백분율 [%]
   - shared: 여러 프로세스가 동시에 액세스 할 수 있는 메모리
     - value: 값 [실수 (float)]
     - unit: 단위 [문자열]
     - raw: 가공되지 않은 정보 [Byte]
   - total: 전체 크기
     - value: 값 [실수 (float)]
     - unit: 단위 [문자열]
     - raw: 가공되지 않은 정보 [Byte]
   - used: 사용 된 크기
     - value: 값 [실수 (float)]
     - unit: 단위 [문자열]
     - raw: 가공되지 않은 정보 [Byte]
 - disk
   - io: 입출력 정보
     - busy_time: 실제 I/O에 소요 된 시간 [ms]
     - read_bytes: 읽은 바이트 수
       - value: 값 [실수 (float)]
       - unit: 단위 [문자열]
       - raw: 가공되지 않은 정보 [Byte]
     - read_count: 읽기 횟수 [횟수]
     - read_merged: 병합된 읽기 수 [횟수]
     - read_time: 읽는데 소요된 시간 [ms]
     - write_bytes: 쓰여진 바이트 수
       - value: 값 [실수 (float)]
       - unit: 단위 [문자열]
       - raw: 가공되지 않은 정보 [Byte]
     - write_count: 쓰기 횟수 [횟수]
     - write_merged: 병합된 쓰기 수 [횟수]
     - write_time: 쓰는데 소요된 시간 [ms (밀리 초)]
   - partition: 파티션 및 장치 정보 (배열)
     - name: 장치 경로 [디렉터리]
     - info: 세부 정보
       - fs_type: 파티션 파일 시스템 [문자열]
       - mount_point: 마운트 위치 [디렉터리]
       - opts: 다른 마운트 옵션 [문자열]
   - usage: 사용량
     - free: 사용 가능한 크기
       - value: 값 [실수 (float)]
       - unit: 단위 [문자열]
       - raw: 가공되지 않은 정보 [Byte]
     - percent: 백분율 [%]
     - total: 전체 크기
       - value: 값 [실수 (float)]
       - unit: 단위 [문자열]
       - raw: 가공되지 않은 정보 [Byte]
     - used: 사용된 크기
       - value: 값 [실수 (float)]
       - unit: 단위 [문자열]
       - raw: 가공되지 않은 정보 [Byte]
 - device (배열)
   - kind: 장치 종류 [문자열]
   - info: 장치 목록 (배열)
     - name: 이름 [문자열]
     - detail: 설명 [문자열]
 - network
   - connection: 연결 정보 (배열)
     - address: 로컬 주소
       - local: IP 주소 [IP 주소 (문자열)]
       - port: 포트 번호 [포트 번호]
     - remote: 원격 주소
       - local: IP 주소 [IP 주소 (문자열)]
       - port: 포트 번호 [포트 번호]
     - family: 주소 패밀리 (AF_INET, AF_INET6, AF_UNIX)
     - fd: 파일 디스크립터 (0, 양수)
     - pid: 소켓을 연 프로세스의 PID [PID 값]
     - status: 상태 [문자열]
     - type: 주소 유형 (SOCK_STREAM, SOCK_DGRAM, SOCK_SEQPACKET)
   - interface: 인터페이스 정보
     - address: 주소 관련 정보 (배열)
       - name: 네트워크 인터페이스 카드 (NIC) 이름 [문자열]
       - info: 세부 정보
         - address: 기본 NIC 주소 [IP 주소 (문자열)]
         - broadcast: 브로드캐스트 [IP 주소 (문자열)]
         - family: 주소 패밀리 (AF_INET, AF_INET6, AF_LINK)
         - netmask: 서브넷 마스크 [IP 주소 (문자열)]
         - ptp: Point To Point 인터페이스의 목표 지점 [IP 주소 (문자열)]
     - status: 상태 관련 정보 (배열)
       - name: 이름 [문자열]
       - info: 세부 정보
         - duplex: 이중 통신 방식 (UNKNOWN, HALF, FULL)
         - isup: NIC 실행 여부[boolean]
         - mtu: NIC 최대 전송 단위 [Byte]
         - speed: NIC 속도 [Mb (메가 비트)]
   - io: 송수신 정보 (배열)
     - name: 카드 이름 [문자열]
     - info: 세부 정보
       - bytes: 바이트 수
         - send: 송신 [개수]
         - receive: 수신 [개수]
       - packets: 패킷 수
         - send: 송신 [개수]
         - receive: 수신 [개수]
       - drop: 총 삭제 수
         - in: 수신 [개수]
         - out: 송신 [개수]
       - error: 총 오류 수
         - in: 수신 [개수]
         - out: 송신 [개수]
 - wifi (배열)
   - name: 장치 이름 [문자열]
   - essid: 네트워크 이름 (Extended SSID) [문자열]
   - info: 세부 정보
     - discarded: 폐기된 패킷 수
       - crypt: 해독 할 수없는 패킷 [개수]
       - id: 유효하지 않은 네트워크 ID [개수]
       - misc: 기타 [개수]
     - quality: 품질 관련 정보
       - level: 신호 강도 [dBm]
       - percent: 신호 품질 백분율 [%]
       - noise: 잡음 [dBm]
     - mode: 작동 상태 (하위 문단에서 설명) [문자열]
     - frequency: 주파수
       - value: 값 [실수 (float)]
       - unit: 단위 [문자열]
       - raw: 가공되지 않은 정보 (value, unit과 같음)
     - access: 액세스 포인트 [AP의 MAC주소]
     - rate: 비트 레이트
       - value: 값 [실수 (float)]
       - unit: 단위 [문자열]
       - raw: 가공되지 않은 정보 (value, unit과 같음)
 - internet
   - download: 다운로드 속도
     - value: 값 [실수 (float)]
     - unit: 단위 [문자열]
     - raw: 가공되지 않은 정보 [bps]
   - upload: 업로드 속도
     - value: 값 [실수 (float)]
     - unit: 단위 [문자열]
     - raw: 가공되지 않은 정보 [bps]
   - ping: 핑 속도 [ms (밀리 초)]
   - server: 서버 정보
     - cc: 국가 코드 [문자열]
     - country: 국가 [문자열]
     - city: 지역 [문자열]
     - sponsor: 서버 제공자 [문자열]
     - url: 서버 주소 [도메인 (문자열)]
   - client: 클라이언트 정보
     - country: 국가 [문자열]
     - ip: IP 주소 [IP 주소 (문자열)]
     - isp: 통신사 [문자열]
   - time: 마지막 측정 시각
     - value: (보기 편하게 수정된) 값 [년-월-일 시:분:초]
     - raw: Raw Data [초]
 - time
   - boot: 부팅 시각
     - value: (보기 편하게 수정된) 값 [년-월-일 시:분:초]
     - raw: Raw Data [초]
   - now: 현재 시각
     - value: (보기 편하게 수정된) 값 [년-월-일 시:분:초]
     - raw: Raw Data [초]
   - passed: 흐른 시간
     - value: (보기 편하게 수정된) 값 [년-월-일 시:분:초]
     - raw: Raw Data [초]


#### wifi - info - mode 상태 설명


 - Ad-Hoc: network composed of only one cell and without Access Point
 - Managed: node connects to a network composed of many Access Points, with roaming
 - Master: the node is the synchronisation master or acts as an Access Point
 - Repeater: the node forwards packets between other wireless nodes
 - Secondary: the node acts as a backup master/repeater
 - Monitor: the node is not associated with any cell and
            passively monitor all packets on the frequency
 - Auto


### Topic - /system_monitor\_simple


자세한 내용이 아니라 요약된 정보가 필요할 때는
/system_monitor\_simple 토픽을 사용하면 된다.

그 외의 내용은 /system_monitor\_simple 토픽과 같다.

 - cpu
   - percent: 사용률 [%]
   - frequency: 주파수
     - current: 현재 [MHz]
     - max: 최대 [MHz]
     - min: 최소 [MHz]
 - ram
   - total: 전체 크기 [문자열]
   - available: 즉시 사용 가능한 크기 [문자열]
   - free: 여유 공간 [문자열]
   - used: 사용 된 크기 [문자열]
   - percent: 사용량 백분율 [%]
 - disk
   - free: 사용 가능한 크기 [문자열]
   - percent: 백분율 [%]
   - total: 전체 크기 [문자열]
   - used: 사용된 크기 [문자열]
 - network
   - interface: NIC 정보 (배열)
     - name: 카드 이름 [문자열]
     - address: 기본 NIC 주소 (배열) [IP 주소 (문자열)]
     - netmask: 서브넷 마스크 주소 (배열) [IP 주소 (문자열)]
   - io: 송수신 정보 (배열)
     - name: 카드 이름 [문자열]
     - send: 송신 패킷 수 [개수]
     - receive: 수신 패킷 수 [개수]
     - drop: 총 삭제 수 [개수]
     - error: 총 오류 수 [개수]
 - wifi
   - name: 장치 이름 [문자열]
   - essid: 네트워크 이름 (Extended SSID) [문자열]
   - mode: 작동 상태 (이전 문단의 내용과 동일) [문자열]
   - frequency: 주파수 [문자열]
   - level: 신호 강도 [dBm]
   - percent: 신호 품질 백분율 [%]
   - noise: 잡음 [dBm]
 - internet
   - download: 다운로드 속도 [문자열]
   - upload: 업로드 속도 [문자열]
   - ping: 핑 속도 [ms (밀리 초)]
   - time: 마지막 측정 시각 [년-월-일 시:분:초]
 - time
   - boot: 부팅 시각 [년-월-일 시:분:초]
   - passed: 흐른 시간 [시:분:초]
   
