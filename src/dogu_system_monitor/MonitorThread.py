import rospy
import datetime
import Monitoring
from Timer import Timer
from dogu_system_monitor.msg import *
from threading import Thread


class Monitor:
    def __init__(self):
        self.monitoring_data = MonitoringInfo()
        self.simple_data = SimpleInfo()
        self.delay = 0.5

        for target in [self.update_cpu, self.update_disk, self.update_device, self.update_network, self.update_ram,
                       self.update_wifi, self.update_internet, self.update_time, self.update_simple]:
            thread = Thread(target=target, args=())
            thread.setDaemon(True)
            thread.start()

    def update_cpu(self):
        timer = Timer()
        while True:
            timer.set()
            data = Monitoring.cpu()

            cpu = CPU()
            if data is not None:
                cpu.percent = round(data["percent"], 2)
                cpu.frequency = Frequency()
                cpu.frequency.current = round(data["frequency"]["current"], 2)
                cpu.frequency.max = round(data["frequency"]["max"], 2)
                cpu.frequency.min = round(data["frequency"]["min"], 2)
                cpu.core = list()
                for i in range(len(data["core"])):
                    cpu.core.append(Core())
                    cpu.core[i].percent = round(data["core"][i]["percent"], 2)
                    cpu.core[i].frequency = Frequency()
                    cpu.core[i].frequency.current = round(data["core"][i]["frequency"]["current"], 2)
                    cpu.core[i].frequency.max = round(data["core"][i]["frequency"]["max"], 2)
                    cpu.core[i].frequency.min = round(data["core"][i]["frequency"]["min"], 2)

            self.monitoring_data.cpu = cpu
            timer.active(self.delay)

    def update_disk(self):
        timer = Timer()
        while True:
            timer.set()
            data = Monitoring.disk()

            disk = Disk()
            if data is not None:
                disk.io = DiskIO()
                disk.io.busy_time = data["io"]["busy_time"]
                disk.io.read_bytes = ValueUnit(round(data["io"]["read_bytes"][1][0], 2),
                                               data["io"]["read_bytes"][1][1], data["io"]["read_bytes"][0])
                disk.io.read_count = data["io"]["read_count"]
                disk.io.read_merged = data["io"]["read_merged"]
                disk.io.read_time = data["io"]["read_time"]
                disk.io.write_bytes = ValueUnit(round(data["io"]["write_bytes"][1][0], 2),
                                                data["io"]["write_bytes"][1][1], data["io"]["write_bytes"][0])
                disk.io.write_count = data["io"]["write_count"]
                disk.io.write_merged = data["io"]["write_merged"]
                disk.io.write_time = data["io"]["write_time"]
                disk.partition = list()
                i = 0
                for key in data["partition"].keys():
                    disk.partition.append(Partition())
                    disk.partition[i].name = key
                    disk.partition[i].info = PartitionInfo()
                    disk.partition[i].info.fs_type = data["partition"][key]["fstype"]
                    disk.partition[i].info.mount_point = data["partition"][key]["mountpoint"]
                    disk.partition[i].info.opts = data["partition"][key]["opts"]
                    i += 1
                disk.usage = Usage()
                disk.usage.free = ValueUnit(round(data["usage"]["free"][1][0], 2), data["usage"]["free"][1][1],
                                            data["usage"]["free"][0])
                disk.usage.percent = round(data["usage"]["percent"], 2)
                disk.usage.total = ValueUnit(round(data["usage"]["total"][1][0], 2), data["usage"]["total"][1][1],
                                             data["usage"]["total"][0])
                disk.usage.used = ValueUnit(round(data["usage"]["used"][1][0], 2), data["usage"]["used"][1][1],
                                            data["usage"]["used"][0])

            self.monitoring_data.disk = disk
            timer.active(self.delay)

    def update_device(self):
        timer = Timer()
        while True:
            timer.set()
            data = Monitoring.device()

            device = list()
            keys = data.keys()
            for i in range(len(keys)):
                device.append(Device())
                device[i].kind = keys[i]
                device[i].info = list()
                for j in range(len(data[keys[i]])):
                    device[i].info.append(DeviceInfo())
                    device[i].info[j].name = data[keys[i]][j]["name"]
                    device[i].info[j].detail = data[keys[i]][j]["detail"]

            self.monitoring_data.device = device
            timer.active(self.delay)

    def update_network(self):
        timer = Timer()
        while True:
            timer.set()
            data = Monitoring.network()

            network = Network()
            if data is not None:
                network.connection = list()
                for i in range(len(data["connection"])):
                    network.connection.append(Connection())
                    network.connection[i].address = AddressBundle()
                    network.connection[i].address.local = IP()
                    try:
                        network.connection[i].address.local.ip = data["connection"][i]["address"]["local"]["ip"]
                    except KeyError:
                        pass
                    try:
                        network.connection[i].address.local.port = data["connection"][i]["address"]["local"]["port"]
                    except KeyError:
                        pass
                    network.connection[i].address.remote = IP()
                    try:
                        network.connection[i].address.remote.ip = data["connection"][i]["address"]["remote"]["ip"]
                    except KeyError:
                        pass
                    try:
                        network.connection[i].address.remote.port = data["connection"][i]["address"]["remote"]["port"]
                    except KeyError:
                        pass
                    network.connection[i].family = data["connection"][i]["family"]
                    if network.connection[i].family is None:
                        network.connection[i].family = 0
                    network.connection[i].fd = data["connection"][i]["fd"]
                    if network.connection[i].fd is None:
                        network.connection[i].fd = 0
                    network.connection[i].pid = data["connection"][i]["pid"]
                    if network.connection[i].pid is None:
                        network.connection[i].pid = 0
                    network.connection[i].status = data["connection"][i]["status"]
                    if network.connection[i].status is None:
                        network.connection[i].status = ""
                    network.connection[i].type = data["connection"][i]["type"]
                    if network.connection[i].type is None:
                        network.connection[i].type = 0
                network.interface = Interface()
                network.interface.address = list()
                i = 0
                for key in data["interface"]["address"].keys():
                    network.interface.address.append(IF_Address())
                    network.interface.address[i].name = key
                    network.interface.address[i].info = list()
                    for j in range(len(data["interface"]["address"][key])):
                        network.interface.address[i].info.append(AddressInfo())
                        network.interface.address[i].info[j].address = data["interface"]["address"][key][j]["address"]
                        if network.interface.address[i].info[j].address is None:
                            network.interface.address[i].info[j].address = ""
                        network.interface.address[i].info[j].broadcast = data["interface"]["address"][key][j][
                            "broadcast"]
                        if network.interface.address[i].info[j].broadcast is None:
                            network.interface.address[i].info[j].broadcast = ""
                        network.interface.address[i].info[j].family = data["interface"]["address"][key][j]["family"]
                        if network.interface.address[i].info[j].family is None:
                            network.interface.address[i].info[j].family = 0
                        network.interface.address[i].info[j].netmask = data["interface"]["address"][key][j]["netmask"]
                        if network.interface.address[i].info[j].netmask is None:
                            network.interface.address[i].info[j].netmask = ""
                        network.interface.address[i].info[j].ptp = data["interface"]["address"][key][j]["ptp"]
                        if network.interface.address[i].info[j].ptp is None:
                            network.interface.address[i].info[j].ptp = ""
                    i += 1
                network.interface.status = list()
                i = 0
                for key in data["interface"]["status"].keys():
                    network.interface.status.append(IF_Status())
                    network.interface.status[i].name = key
                    network.interface.status[i].info = StatusInfo()
                    network.interface.status[i].info.duplex = data["interface"]["status"][key]["duplex"]
                    network.interface.status[i].info.isup = data["interface"]["status"][key]["isup"]
                    network.interface.status[i].info.mtu = data["interface"]["status"][key]["mtu"]
                    network.interface.status[i].info.speed = data["interface"]["status"][key]["speed"]
                    i += 1
                network.io = list()
                i = 0
                for key in data["io"].keys():
                    network.io.append(IO())
                    network.io[i].name = key
                    network.io[i].info = IOInfo()
                    network.io[i].info.bytes = SendReceive()
                    network.io[i].info.bytes.send = data["io"][key]["bytes"]["send"]
                    network.io[i].info.bytes.receive = data["io"][key]["bytes"]["receive"]
                    network.io[i].info.packets = SendReceive()
                    network.io[i].info.packets.send = data["io"][key]["packets"]["send"]
                    network.io[i].info.packets.receive = data["io"][key]["packets"]["receive"]
                    network.io[i].info.drop = InOut()
                    network.io[i].info.drop.In = data["io"][key]["drop"]["in"]
                    network.io[i].info.drop.Out = data["io"][key]["drop"]["out"]
                    network.io[i].info.error = InOut()
                    network.io[i].info.error.In = data["io"][key]["error"]["in"]
                    network.io[i].info.error.Out = data["io"][key]["error"]["out"]
                    i += 1

            self.monitoring_data.network = network
            timer.active(self.delay)

    def update_ram(self):
        timer = Timer()
        while True:
            timer.set()
            data = Monitoring.ram()

            ram = RAM()
            if data is not None:
                ram.active = ValueUnit(round(data["active"][1][0], 2), data["active"][1][1], data["active"][0])
                ram.available = ValueUnit(round(data["available"][1][0], 2), data["available"][1][1],
                                          data["available"][0])
                ram.buffers = ValueUnit(round(data["buffers"][1][0], 2), data["buffers"][1][1], data["buffers"][0])
                ram.cached = ValueUnit(round(data["cached"][1][0], 2), data["cached"][1][1], data["cached"][0])
                ram.free = ValueUnit(round(data["free"][1][0], 2), data["free"][1][1], data["free"][0])
                ram.inactive = ValueUnit(round(data["inactive"][1][0], 2), data["inactive"][1][1], data["inactive"][0])
                ram.percent = round(data["percent"], 2)
                ram.shared = ValueUnit(round(data["shared"][1][0], 2), data["shared"][1][1], data["shared"][0])
                ram.total = ValueUnit(round(data["total"][1][0], 2), data["total"][1][1], data["total"][0])
                ram.used = ValueUnit(round(data["used"][1][0], 2), data["used"][1][1], data["used"][0])

            self.monitoring_data.ram = ram
            timer.active(self.delay)

    def update_wifi(self):
        timer = Timer()
        while True:
            timer.set()
            data = Monitoring.wifi()

            wifi = list()
            if data is not None:
                i = 0
                for key in data:
                    wifi.append(WiFi())
                    wifi[i].name = key
                    wifi[i].essid = data[key]["essid"]
                    wifi[i].info = WiFiInfo()
                    wifi[i].info.discarded = Discarded()
                    wifi[i].info.discarded.crypt = data[key]["discarded"]["crypt"]
                    wifi[i].info.discarded.id = data[key]["discarded"]["id"]
                    wifi[i].info.discarded.misc = data[key]["discarded"]["misc"]
                    wifi[i].info.quality = Quality()
                    wifi[i].info.quality.level = data[key]["quality"]["level"]
                    wifi[i].info.quality.percent = round(data[key]["quality"]["link"], 2)
                    wifi[i].info.quality.noise = data[key]["quality"]["noise"]
                    wifi[i].info.mode = data[key]["mode"]
                    wifi[i].info.frequency = ValueUnit(round(data[key]["freq"]["value"], 2), data[key]["freq"]["unit"],
                                                       data[key]["freq"]["value"])
                    wifi[i].info.access = data[key]["access"]
                    wifi[i].info.rate = ValueUnit(data[key]["bitrate"]["value"], data[key]["bitrate"]["unit"],
                                                  data[key]["bitrate"]["value"])
                    i += 1

            self.monitoring_data.wifi = wifi
            timer.active(self.delay)

    def update_internet(self):
        timer = Timer()
        while True:
            timer.set()
            data = Monitoring.internet()

            internet = Internet()
            if data is not None:
                internet.download = ValueUnit(round(data["download"][1][0], 2), data["download"][1][1],
                                              data["download"][0])
                internet.upload = ValueUnit(round(data["upload"][1][0], 2), data["upload"][1][1],
                                            data["upload"][0])
                internet.ping = round(data["ping"], 2)
                internet.server = InternetServer()
                internet.server.cc = data["server"]["cc"]
                internet.server.country = data["server"]["country"]
                internet.server.city = data["server"]["name"]
                internet.server.sponsor = data["server"]["sponsor"]
                internet.server.url = data["server"]["url"]
                internet.client = InternetClient()
                internet.client.country = data["client"]["country"]
                internet.client.ip = data["client"]["ip"]
                internet.client.isp = data["client"]["isp"]
                internet.time = ValueRaw()
                internet.time.value = datetime.datetime.fromtimestamp(data["time"]).strftime("%Y-%m-%d %H:%M:%S")
                internet.time.raw = data["time"]

            self.monitoring_data.internet = internet
            timer.active(60)

    def update_time(self):
        timer = Timer()
        while True:
            timer.set()
            data = Monitoring.time()

            time = Time()
            if data is not None:
                time.boot = ValueRaw()
                time.boot.value = datetime.datetime.fromtimestamp(data["boot"]).strftime("%Y-%m-%d %H:%M:%S")
                time.boot.raw = data["boot"]
                time.now = ValueRaw()
                time.now.value = datetime.datetime.fromtimestamp(data["now"]).strftime("%Y-%m-%d %H:%M:%S")
                time.now.raw = data["now"]
                time.passed = ValueRaw()
                time.passed.value = str(datetime.timedelta(seconds=data["passed"]))
                time.passed.raw = data["passed"]

            self.monitoring_data.time = time
            timer.active(self.delay)

    def update_simple(self):
        timer = Timer()
        while True:
            timer.set()
            data = Monitoring.time()

            simple = SimpleInfo()
            simple.cpu = Core()
            simple.cpu.percent = round(self.monitoring_data.cpu.percent, 2)
            simple.cpu.frequency = self.monitoring_data.cpu.frequency
            simple.ram = SimpleRAM()
            simple.ram.total = str(round(self.monitoring_data.ram.total.value, 2)) + \
                               " [" + str(self.monitoring_data.ram.total.unit) + "]"
            simple.ram.available = str(round(self.monitoring_data.ram.available.value, 2)) + \
                                   " [" + str(self.monitoring_data.ram.available.unit) + "]"
            simple.ram.free = str(round(self.monitoring_data.ram.free.value, 2)) + \
                              " [" + str(self.monitoring_data.ram.free.unit) + "]"
            simple.ram.used = str(round(self.monitoring_data.ram.used.value, 2)) + \
                              " [" + str(self.monitoring_data.ram.used.unit) + "]"
            simple.ram.percent = round(self.monitoring_data.ram.percent, 2)
            simple.disk = SimpleDisk()
            simple.disk.free = str(round(self.monitoring_data.disk.usage.free.value, 2)) + \
                               " [" + str(self.monitoring_data.disk.usage.free.unit) + "]"
            simple.disk.percent = round(self.monitoring_data.disk.usage.percent, 2)
            simple.disk.total = str(round(self.monitoring_data.disk.usage.total.value, 2)) + \
                                " [" + str(self.monitoring_data.disk.usage.total.unit) + "]"
            simple.disk.used = str(round(self.monitoring_data.disk.usage.used.value, 2)) + \
                               " [" + str(self.monitoring_data.disk.usage.used.unit) + "]"
            simple.network = SimpleNW()
            simple.network.interface = list()
            for i in range(len(self.monitoring_data.network.interface.address)):
                simple.network.interface.append(SimpleIF())
                simple.network.interface[i].name = self.monitoring_data.network.interface.address[i].name
                simple.network.interface[i].address = list()
                simple.network.interface[i].netmask = list()
                for info in self.monitoring_data.network.interface.address[i].info:
                    simple.network.interface[i].address.append(info.address)
                    simple.network.interface[i].netmask.append(info.netmask)
                    simple.network.io = list()
            for i in range(len(self.monitoring_data.network.io)):
                simple.network.io.append(SimpleIO())
                simple.network.io[i].name = self.monitoring_data.network.io[i].name
                simple.network.io[i].send = self.monitoring_data.network.io[i].info.packets.send
                simple.network.io[i].receive = self.monitoring_data.network.io[i].info.packets.receive
                simple.network.io[i].drop = self.monitoring_data.network.io[i].info.drop.In + \
                                            self.monitoring_data.network.io[i].info.drop.Out
                simple.network.io[i].error = self.monitoring_data.network.io[i].info.error.In + \
                                             self.monitoring_data.network.io[i].info.error.Out
            simple.wifi = list()
            for i in range(len(self.monitoring_data.wifi)):
                simple.wifi.append(SimpleWiFi())
                simple.wifi[i].name = self.monitoring_data.wifi[i].name
                simple.wifi[i].essid = self.monitoring_data.wifi[i].essid
                simple.wifi[i].mode = self.monitoring_data.wifi[i].info.mode
                simple.wifi[i].frequency = str(round(self.monitoring_data.wifi[i].info.frequency.value, 2)) + \
                                           " [" + str(self.monitoring_data.wifi[i].info.frequency.unit) + "]"
                simple.wifi[i].level = self.monitoring_data.wifi[i].info.quality.level
                simple.wifi[i].percent = round(self.monitoring_data.wifi[i].info.quality.percent, 2)
                simple.wifi[i].noise = self.monitoring_data.wifi[i].info.quality.noise
            simple.internet = SimpleNet()
            simple.internet.download = str(round(self.monitoring_data.internet.download.value, 2)) + \
                                       " [" + str(self.monitoring_data.internet.download.unit) + "]"
            simple.internet.upload = str(round(self.monitoring_data.internet.upload.value, 2)) + \
                                     " [" + str(self.monitoring_data.internet.upload.unit) + "]"
            simple.internet.ping = self.monitoring_data.internet.ping
            simple.internet.time = self.monitoring_data.internet.time.value
            simple.time = SimpleTime()
            simple.time.boot = self.monitoring_data.time.boot.value
            simple.time.passed = self.monitoring_data.time.passed.value

            self.simple_data = simple
            timer.active(self.delay)
