import time


class Timer:
    def __init__(self):
        self.start = 0.0

    def set(self):
        self.start = time.time()

    def active(self, delay):
        try:
            time.sleep(self.start + delay - time.time())
        except IOError:
            pass
