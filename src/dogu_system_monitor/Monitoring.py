import re
from subprocess import check_output

import psutil
import speedtest


def atoi(string):
    return int(re.findall(r"^[-+]?[0-9]+", string)[0])


def unit_change(value, unit, unit_size):
    result = [value, [value, unit]]

    for si in ["K", "M", "G", "T", "P", "E", "Z", "Y"]:
        if result[1][0] >= unit_size:
            result[1][0] /= float(unit_size)
            result[1][1] = si + unit
        else:
            break

    return result


def cpu():
    def scpufreq_to_list(scpufreq):
        return {"current": scpufreq.current,
                "max": scpufreq.max,
                "min": scpufreq.min}

    result = {"percent": psutil.cpu_percent(interval=1),
              "frequency": scpufreq_to_list(psutil.cpu_freq())}

    percent_list = psutil.cpu_percent(interval=1, percpu=True)
    frequency_list = psutil.cpu_freq(percpu=True)

    result["core"] = [dict() for i in range(psutil.cpu_count())]

    for i in range(len(result["core"])):
        result["core"][i] = {"percent": percent_list[i],
                             "frequency": scpufreq_to_list(frequency_list[i])}

    return result


def ram():
    stat = psutil.virtual_memory()
    return {"total": unit_change(stat.total, "B", 1024),
            "available": unit_change(stat.available, "B", 1024),
            "percent": stat.percent,
            "used": unit_change(stat.used, "B", 1024),
            "free": unit_change(stat.free, "B", 1024),
            "active": unit_change(stat.active, "B", 1024),
            "inactive": unit_change(stat.inactive, "B", 1024),
            "buffers": unit_change(stat.buffers, "B", 1024),
            "cached": unit_change(stat.cached, "B", 1024),
            "shared": unit_change(stat.shared, "B", 1024)}


def disk():
    result = None

    partitions = psutil.disk_partitions(all=True)
    usage = psutil.disk_usage('/')

    try:
        io = psutil.disk_io_counters(perdisk=False)
    except ValueError:
        io = None

    if io:
        partitions_list = dict()
        for sdiskpart in partitions:
            partitions_list[sdiskpart.device] = {"mountpoint": sdiskpart.mountpoint,
                                                 "fstype": sdiskpart.fstype,
                                                 "opts": sdiskpart.opts}

        result = {"partition": partitions_list,
                  "usage": {"total": unit_change(usage.total, "B", 1000),
                            "used": unit_change(usage.used, "B", 1000),
                            "free": unit_change(usage.free, "B", 1000),
                            "percent": usage.percent},
                  "io": {"read_count": io.read_count,
                         "write_count": io.write_count,
                         "read_bytes": unit_change(io.read_bytes, "B", 1000),
                         "write_bytes": unit_change(io.write_bytes, "B", 1000),
                         "read_time": io.read_time,
                         "write_time": io.write_time,
                         "read_merged": io.read_merged_count,
                         "write_merged": io.write_merged_count,
                         "busy_time": io.busy_time}}

    return result


def device():
    result = dict()

    cmd = check_output(["hwinfo", "--short"], universal_newlines=True)
    regex = re.findall(r"^(.+):|^\s{2}([\w\/]*)\s{3,}(.+)$", cmd, re.MULTILINE)

    for tmp in regex:
        if tmp[0] is not "":
            last = tmp[0]
            result[last] = list()
            continue

        result[last].append({"name": tmp[1], "detail": tmp[2]})

    return result


def network():
    io = psutil.net_io_counters(pernic=True)
    connection = psutil.net_connections()
    if_addr = psutil.net_if_addrs()
    if_stat = psutil.net_if_stats()

    io_list = dict()
    for key in io.keys():
        io_list[key] = {"bytes": {"send": io[key].bytes_sent,
                                  "receive": io[key].bytes_recv},
                        "packets": {"send": io[key].packets_sent,
                                    "receive": io[key].packets_recv},
                        "error": {"in": io[key].errin,
                                  "out": io[key].errout},
                        "drop": {"in": io[key].dropin,
                                 "out": io[key].dropout}}

    def addr_tuple_to_dict(addr):
        result = dict()

        if len(addr) > 0:
            result["ip"] = addr[0]

        if len(addr) > 1:
            result["port"] = addr[1]

        return result

    connection_list = list()
    for sconn in connection:
        connection_list.append({"fd": sconn.fd,
                                "family": sconn.family,
                                "type": sconn.type,
                                "address": {"local": addr_tuple_to_dict(sconn.laddr),
                                            "remote": addr_tuple_to_dict(sconn.raddr)},
                                "status": sconn.status,
                                "pid": sconn.pid})

    interface_list = dict()

    interface_list["address"] = dict()
    for key in if_addr.keys():
        interface_list["address"][key] = list()

        for snicaddr in if_addr[key]:
            interface_list["address"][key].append({"family": snicaddr.family,
                                                   "address": snicaddr.address,
                                                   "netmask": snicaddr.netmask,
                                                   "broadcast": snicaddr.broadcast,
                                                   "ptp": snicaddr.ptp})

    interface_list["status"] = dict()
    for key in if_stat.keys():
        interface_list["status"][key] = {"isup": if_stat[key].isup,
                                         "speed": if_stat[key].speed,
                                         "mtu": if_stat[key].mtu}
        if if_stat[key].duplex == 0:
            interface_list["status"][key]["duplex"] = "UNKNOWN"
        elif if_stat[key].duplex == 1:
            interface_list["status"][key]["duplex"] = "HALF"
        elif if_stat[key].duplex == 2:
            interface_list["status"][key]["duplex"] = "FULL"

    return {"io": io_list,
            "connection": connection_list,
            "interface": interface_list}


def wifi():
    result = dict()

    with open("/proc/net/wireless", "r") as f:
        stat = f.readlines()

    for i in range(2, len(stat)):
        array = stat[i].split()

        result[array[0][:-1]] = {"quality": {"level": atoi(array[3]),
                                             "noise": atoi(array[4])},
                                 "discarded": {"id": atoi(array[5]),
                                               "crypt": atoi(array[6]),
                                               "misc": atoi(array[7])}}

    for key in result.keys():
        cmd = check_output(["iwconfig", key], universal_newlines=True)
        regex = re.findall(
            r"ESSID[:=]\s*\"([\w\s.]+)\"|Mode[:=]\s*(\w+)|Frequency[:=]\s*([\d.]+)\s*(\w+)|Access\sPoint[:=]\s*([\w:]+)|Bit\sRate[:=]([\d.]+)\s*([\w\/]+)|Link\sQuality[:=](\d+)\/(\d+)",
            cmd, re.MULTILINE)

        for tmp in regex:
            if tmp[0] is not "":  result[key]["essid"] = tmp[0]
            if tmp[1] is not "":  result[key]["mode"] = tmp[1]
            if tmp[2] is not "":  result[key]["freq"] = {"value": float(tmp[2]), "unit": tmp[3]}
            if tmp[4] is not "":  result[key]["access"] = tmp[4]
            if tmp[5] is not "":  result[key]["bitrate"] = {"value": float(tmp[5]), "unit": tmp[6]}
            if tmp[7] is not "":  result[key]["quality"]["link"] = float(tmp[7]) / float(tmp[8]) * 100

        if "essid" not in result[key].keys():  result[key]["essid"] = ""
        if "mode" not in result[key].keys():  result[key]["mode"] = ""
        if "freq" not in result[key].keys():  result[key]["freq"] = {"value": 0.0, "unit": ""}
        if "access" not in result[key].keys():  result[key]["access"] = ""
        if "bitrate" not in result[key].keys():  result[key]["bitrate"] = {"value": 0.0, "unit": ""}

    return result


def internet():
    result = None

    try:
        s = speedtest.Speedtest()
        s.get_closest_servers()
        s.download()
        s.upload()

        result = s.results.dict()
        result["bytes_sent"] = unit_change(result["bytes_sent"], "B", 1000)
        result["bytes_received"] = unit_change(result["bytes_received"], "B", 1000)
        result["download"] = unit_change(result["download"], "bps", 1000)
        result["upload"] = unit_change(result["upload"], "bps", 1000)
        result["time"] = psutil.time.time()
    except speedtest.ConfigRetrievalError:
        pass

    return result


def time():
    boot = psutil.boot_time()
    now = psutil.time.time()

    return {"boot": boot,
            "now": now,
            "passed": now - boot}
