#!/usr/bin/env python

import rospy
from dogu_system_monitor import *
from dogu_system_monitor.msg import *


def system_monitor():
    rospy.init_node('system_monitor')

    monitor = Monitor()
    timer = Timer()

    pub = rospy.Publisher('system_monitor', MonitoringInfo, queue_size=10)
    pub_simple = rospy.Publisher('system_monitor_simple', SimpleInfo, queue_size=10)

    while not rospy.is_shutdown():
        timer.set()

        pub.publish(monitor.monitoring_data)
        pub_simple.publish(monitor.simple_data)

        freq = float(rospy.get_param('system_monitoring_frequency'))
        delay = 1 / freq

        monitor.delay = delay / 2.0
        print("[Publish] Delay: {}sec / Frequency: {}Hz".format(delay, freq))

        timer.active(delay)


if __name__ == '__main__':
    try:
        system_monitor()
    except rospy.ROSInterruptException:
        pass
